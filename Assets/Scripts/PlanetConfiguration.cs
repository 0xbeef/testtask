﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlanetConfiguration", menuName = "BallGame/PlanetConfiguration", order = 0)]
public class PlanetConfiguration : ScriptableObject
{
    [SerializeField]
    [Range(0, 10)]
    float gravity;
    [SerializeField]
    Color color;   

    public float Gravity { get { return gravity; } }
    public Color Color { get { return color; } }
}