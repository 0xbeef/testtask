﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BallScript : MonoBehaviour
{
    Rigidbody2D m_rigidbody;
    UnityEvent m_ballHitEvent = new UnityEvent();

    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
    }

    public void UpdateGravity(float gravity)
    {
        m_rigidbody.gravityScale = gravity;
    }

    public void AddHitListener(UnityAction action)
    {
        m_ballHitEvent.AddListener(action);
    }
   
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            dir = dir.normalized * 10f;
            m_rigidbody.velocity = new Vector2(dir.x, m_rigidbody.velocity.y);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        m_ballHitEvent.Invoke();
    }
}
