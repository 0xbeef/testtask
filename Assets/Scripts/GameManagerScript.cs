﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour
{
    [SerializeField]
    Canvas m_canvas;
    [SerializeField]
    GameObject m_ball;
    [SerializeField]
    GameObject m_level;

    int m_ballHitCount = 0;

    void Start()
    {
        m_canvas = Instantiate(m_canvas) as Canvas;
        m_ball = Instantiate(m_ball) as GameObject;
        Instantiate(m_level);

        m_canvas.GetComponent<CanvasScript>().AddConfigurationChangedListener(StartGame);
        m_ball.GetComponent<BallScript>().AddHitListener(OnBallHit);

        ShowMenu();
    }

    void Update() 
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace))    
        {
            ShowMenu();
        }
    }

    void StartGame(PlanetConfiguration config)
    {
        HideMenu();
        Camera.main.backgroundColor = config.Color;
        m_ball.GetComponent<BallScript>().UpdateGravity(config.Gravity);
    }

    void OnBallHit()
    {
        ++m_ballHitCount;
        m_canvas.GetComponent<CanvasScript>().UpdateBallHit(m_ballHitCount);
    }

    void ShowMenu()
    {
        Time.timeScale = 0;
        m_canvas.gameObject.SetActive(true);
    }

    void HideMenu()
    {
        Time.timeScale = 1;
        m_canvas.gameObject.SetActive(false);
    }
}
