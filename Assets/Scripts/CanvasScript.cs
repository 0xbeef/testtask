﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ConfigurationChangedEvent : UnityEvent<PlanetConfiguration> {}

public class CanvasScript : MonoBehaviour
{
    [SerializeField]
    Text m_ballHitText;
    ConfigurationChangedEvent m_configChangedEvent = new ConfigurationChangedEvent();

    public void AddConfigurationChangedListener(UnityAction<PlanetConfiguration> action)
    {
        m_configChangedEvent.AddListener(action);
    }

    public void ConfigurationChanged(PlanetConfiguration config)
    {
        m_configChangedEvent.Invoke(config);
    }

    public void UpdateBallHit(int value)
    {
        m_ballHitText.text = value.ToString();
    }
}
